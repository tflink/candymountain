#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import logging

from flask import Flask, render_template
from flask.ext.sqlalchemy import SQLAlchemy


__author__ = 'Tim Flink'
__email__ = 'tflink@fedoraproject.org'
__version__ = '0.0.1'


# Flask App
app = Flask(__name__)

# load default configuration
if os.getenv('DEV') == 'true':
    app.config.from_object('candymountain.config.DevelopmentConfig')
elif os.getenv('TEST') == 'true':
    app.config.from_object('candymountain.config.TestingConfig')
else:
    app.config.from_object('candymountain.config.ProductionConfig')


# load real configuration values to override defaults
config_file = '/etc/candymountain/settings.py'
if not app.testing:
    if os.path.exists(config_file):
        app.logger.info('Loading configuration from %s' % config_file)
        app.config.from_pyfile(config_file)
    else:
        config_file = os.path.abspath('conf/settings.py')
        if os.path.exists(config_file):
            app.logger.info('Loading configuration from %s' % config_file)
            app.config.from_pyfile(config_file)
        else:
            app.logger.info('No extra config found, using defaults')

# setup logging
fmt = '[%(filename)s:%(lineno)d] ' if app.debug else '%(module)-12s '
fmt += '%(asctime)s %(levelname)-7s %(message)s'
datefmt = '%Y-%m-%d %H:%M:%S'
loglevel = logging.DEBUG if app.debug else logging.INFO
formatter = logging.Formatter(fmt=fmt, datefmt=datefmt)


def setup_logging():
    root_logger = logging.getLogger('')
    root_logger.setLevel(logging.DEBUG)

    if app.config['STREAM_LOGGING']:
        app.logger.debug("doing stream logging")
        stream_handler = logging.StreamHandler()
        stream_handler.setLevel(loglevel)
        stream_handler.setFormatter(formatter)
        root_logger.addHandler(stream_handler)
        app.logger.addHandler(stream_handler)

    if app.config['SYSLOG_LOGGING']:
        app.logger.debug("doing syslog logging")
        syslog_handler = logging.handlers.SysLogHandler(
            address='/dev/log', facility=logging.handlers.SysLogHandler.LOG_LOCAL4)
        syslog_handler.setLevel(loglevel)
        syslog_handler.setFormatter(formatter)
        root_logger.addHandler(syslog_handler)
        app.logger.addHandler(syslog_handler)

    if app.config['FILE_LOGGING'] and app.config['LOGFILE']:
        app.logger.debug("doing file logging to %s" % app.config['LOGFILE'])
        file_handler = logging.handlers.RotatingFileHandler(app.config['LOGFILE'], maxBytes=500000, backupCount=5)
        file_handler.setLevel(loglevel)
        file_handler.setFormatter(formatter)
        root_logger.addHandler(file_handler)
        app.logger.addHandler(file_handler)


setup_logging()

app.logger.debug('using DBURI: %s' % app.config['SQLALCHEMY_DATABASE_URI'])

# database 
db = SQLAlchemy(app)

@app.template_filter('datetime')
def datetime_format(value, format='%Y-%m-%d %H:%M:%S UTC'):
    if value is not None:
        return value.strftime(format)
    return ''


# register blueprints
from . controllers.main import main
app.register_blueprint(main)

# setup error handling
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html', title="Page Not Found!"), 404
