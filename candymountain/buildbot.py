import requests
import json

class BuildbotClient(object):
    def __init__(self):
        pass

    def get(self, url):
        d = requests.get(url)
        return d.text

class BuildbotInterface(object):
    def __init__(self, buildmaster_url, client=None):
        self.buildmaster_url = buildmaster_url

        if client is not None:
            self.client = client
        else:
            self.client = BuildbotClient()


    def make_buildbot_properties_dict(self, properties):
        prop_dict = {}
        for item in properties:
            prop_dict[item[0]] = item[1]

        return prop_dict

    def get_buildbot_data(self, builder, buildid):
        # from the data in the post message from the buildmaster,
        # builder -> buildername
        # buildid -> buildnumber

        raw_json = self.client.get('%s/json/builders/%s/builds/%s' % (self.buildmaster_url, builder, buildid))

        data = json.loads(raw_json)
        properties = self.make_buildbot_properties_dict(data['properties'])

        from pprint import pprint
        pprint(properties)

        return_data = {}
        return_data[u'taskname'] = properties[u'taskname']
        return_data[u'item'] = properties[u'item']
        return_data[u'item_type'] = properties['item_type']
        return_data[u'time_triggered'] = data[u'sourceStamps'][0]['changes'][0]['at']
        return_data[u'slavename'] = properties[u'slavename']
        return_data[u'buildername'] = properties[u'buildername']
        return_data[u'task_revision'] = properties[u'got_revision']

        return return_data


# get taskname, item, item_type, when triggered, got_revision, slavename, builder_name, build_number
