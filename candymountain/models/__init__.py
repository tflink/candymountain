from candymountain import db

class Record(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    taskname = db.Column(db.String)
    item = db.Column(db.String)
    item_type = db.Column(db.String)
    time_triggered = db.Column(db.String)
    slavename = db.Column(db.String)
    buildername = db.Column(db.String)
    task_revision = db.Column(db.String)

    def __init__(self, taskname, item, item_type, time_triggered, slavename,
                    buildername, task_revision):
        self.taskname = taskname
        self.item = item
        self.item_type = item_type
        self.time_triggered = time_triggered
        self.slavename = slavename
        self.buildername = buildername
        self.task_revision = task_revision

