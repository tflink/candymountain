import json

from flask import Blueprint, render_template, request

from candymountain import app, db
from candymountain import buildbot
from candymountain.models import Record
main = Blueprint('main', __name__)

@main.route('/')
def index():
    if app.debug:
        app.logger.debug('rendering index')
    records = Record.query.all()

    return render_template('index.html', title='Candy Mountain', records = records)

@main.route('/build', methods=['POST'])
def process_build():

    if app.debug:
        app.logger.debug('json data posted to /build: %s' % request.data)

    decoded_data = json.loads(request.data)
    buildbot_interface  = buildbot.BuildbotInterface(decoded_data['buildmaster'])
    buildbot_data = buildbot_interface.get_buildbot_data(decoded_data['builder'], decoded_data['buildid'])

#        self.taskname = taskname
#        self.item = item
#        self.item_type = item_type
#        self.time_triggered = time_triggered
#        self.slavename = slavename
#        self.buildername = buildername
#        self.task_revision = task_revision


    newrecord = Record(buildbot_data['taskname'], buildbot_data['item'], buildbot_data['item_type'], buildbot_data['time_triggered'],
                        buildbot_data['slavename'], buildbot_data['buildername'], buildbot_data['task_revision'])
    db.session.add(newrecord)
    db.session.commit()
    return ''

@main.route('/buildbottest', methods = ['POST', 'PUT'])
def buildbot_test():

    from pprint import pprint

    app.logger.debug('data posted to /buildbottest: %s' % request.form)


    return ''
