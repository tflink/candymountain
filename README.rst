===============================
Candy Mountain
===============================

Candy Mountain is a wonderous place where jobs live ... and are dissected to inspect their inner workings

* Free software: GPL2+ license
* Documentation: http://candymountain.rtfd.org.


Configuring buildmaster to send POSTs
-------------------------------------

```
# where http://127.0.0.1:5000/buildbottest is the thing that you want to send POSTs to
import buildbot.status.status_push
sp = buildbot.status.status_push.HttpStatusPush(
        serverUrl="http://127.0.0.1:5000/buildbottest",
        debug=True)
c['status'].append(sp)
```


Example POST data from buildmaster
----------------------------------

Example json in buildmaster post:
```
data posted to /buildbottest

ImmutableMultiDict([
('packets', u'[
  
{
    "event": "stepFinished",
    "id": 163,
    "payload": {
      "properties": [
        [
          "arch",
          "i386",
          "Force Build Form"
        ],
        [
          "branch",
          "",
          "Build"
        ],
        [
          "builddir",
          "/home/tflink/taskotron-dev/slave/i386",
          "slave"
        ],
        [
          "buildername",
          "i386",
          "Builder"
        ],
        [
          "buildnumber",
          21,
          "Build"
        ],
        [
          "codebase",
          "",
          "Build"
        ],
        [
          "got_revision",
          "fe2f78562ef8f9af8ea0e5e1290aab4d957a4a1b",
          "Git"
        ],
        [
          "item",
          "openstack-puppet-modules-2014.1-20.1.fc22",
          "Force Build Form"
        ],
        [
          "item_type",
          "koji_build",
          "Force Build Form"
        ],
        [
          "owner",
          "tasky <tasky@localhost>",
          "Force Build Form"
        ],
        [
          "owners",
          [
            "tasky <tasky@localhost>"
          ],
          "The web-page \'rebuild\' button was pressed by \'tasky <tasky@localhost>\': "
        ],
        [
          "project",
          "",
          "Build"
        ],
        [
          "reason",
          "force build",
          "Force Build Form"
        ],
        [
          "repository",
          "",
          "Build"
        ],
        [
          "revision",
          "fe2f78562ef8f9af8ea0e5e1290aab4d957a4a1b",
          "Build"
        ],
        [
          "scheduler",
          "rpmcheck",
          "Scheduler"
        ],
        [
          "slavename",
          "simon-local",
          "BuildSlave"
        ],
        [
          "taskname",
          "rpmlint",
          "Force Build Form"
        ],
        [
          "workdir",
          "/home/tflink/taskotron-dev/slave/i386",
          "slave (deprecated)"
        ]
      ],
      "step": {
        "expectations": [
         
 [
            "output",
            25460,
            null
          ]
        ],
        "isFinished": true,
        "isStarted": true,
        "logs": [
          [
            "stdio",
            "http://127.0.0.1:8080/builders/i386/builds/21/steps/git/logs/stdio"
          ]
        ],
        "name": "git",
        "step_number": 1,
        "text": [
          "update"
        ],
        "times": [
          1408440023.570055,
          1408440025.600167
        ]
      }
    },
    "project": "Taskotron",
    "started": "2014-08-19 09:06:39.014166",
    "timestamp": "2014-08-19 09:20:25.600401"
  },
  {
    "event": "stepStarted",
    "id": 164,
    "payload": {
      "properties": [
        [
          "arch",
          "i386",
          "Force Build Form"
        ],
        [
          "branch",
          "",
          "Build"
        ],
        [
          "builddir",
          "/home/tflink/taskotron-dev/slave/i386",
          "slave"
        ],
        [
          "buildername",
          "i386",
          "Builder"
        ],
        [
          "buildnumber",
          21,
          "Build"
        ],
        [
          "codebase",
          "",
          "Build"
        ],
```
