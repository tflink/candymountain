import json

from pprint import pprint
from dingus import Dingus

from candymountain import buildbot


simple_pass_json = u'{"blame":["taskotron"],"builderName":"all","currentStep":null,"eta":null,"logs":[["stdio","http://taskotron-dev.fedoraproject.org/taskmaster/builders/all/builds/4513/steps/rm_log/logs/stdio"],["stdio","http://taskotron-dev.fedoraproject.org/taskmaster/builders/all/builds/4513/steps/git/logs/stdio"],["stdio","http://taskotron-dev.fedoraproject.org/taskmaster/builders/all/builds/4513/steps/runtask/logs/stdio"],["stdio","http://taskotron-dev.fedoraproject.org/taskmaster/builders/all/builds/4513/steps/cat_log/logs/stdio"]],"number":4513,"properties":[["arch","noarch","Change"],["branch",null,"Build"],["builddir","/home/buildslave/slave/all","slave"],["buildername","all","Builder"],["buildnumber",4513,"Build"],["codebase","","Build"],["got_revision","2b3a8895897efae1f93cfd94378247d6d9419d8e","Git"],["item","krb5-1.12.1-9.fc21","Change"],["item_type","koji_build","Change"],["project","rpmcheck","Build"],["repository","","Build"],["revision",null,"Build"],["scheduler","jobsched-noarch","Scheduler"],["slavename","taskotron-client28","BuildSlave"],["taskname","rpmlint","Change"],["workdir","/home/buildslave/slave/all","slave (deprecated)"]],"reason":"scheduler","results":0,"slave":"taskotron-client28","sourceStamps":[{"branch":null,"changes":[{"at":"Mon 18 Aug 2014 13:26:55","branch":null,"category":"noarch","codebase":"","comments":"build request from taskotron-trigger","files":[],"number":21183,"project":"rpmcheck","properties":[["arch","noarch","Change"],["item","krb5-1.12.1-9.fc21","Change"],["item_type","koji_build","Change"],["taskname","rpmlint","Change"]],"repository":"","rev":null,"revision":null,"revlink":null,"when":1408368415,"who":"taskotron"}],"codebase":"","hasPatch":false,"project":"rpmcheck","repository":"","revision":null}],"steps":[{"eta":null,"expectations":[],"hidden":false,"isFinished":true,"isStarted":true,"logs":[],"name":"RemoveDirectory","results":[0,[]],"statistics":{},"step_number":0,"text":["Deleted"],"times":[1408368415.947859,1408368416.000681],"urls":{}},{"eta":null,"expectations":[["output",461,461.0]],"hidden":false,"isFinished":true,"isStarted":true,"logs":[["stdio","http://taskotron-dev.fedoraproject.org/taskmaster/builders/all/builds/4513/steps/rm_log/logs/stdio"]],"name":"rm_log","results":[0,[]],"statistics":{},"step_number":1,"text":["\'rm","-f","...\'"],"times":[1408368416.000973,1408368416.05207],"urls":{}},{"eta":null,"expectations":[["output",1893,1893.0]],"hidden":false,"isFinished":true,"isStarted":true,"logs":[["stdio","http://taskotron-dev.fedoraproject.org/taskmaster/builders/all/builds/4513/steps/git/logs/stdio"]],"name":"git","results":[0,[]],"statistics":{},"step_number":2,"text":["update"],"times":[1408368416.052501,1408368416.58158],"urls":{}},{"eta":null,"expectations":[["output",1835,3703.846239271743]],"hidden":false,"isFinished":true,"isStarted":true,"logs":[["stdio","http://taskotron-dev.fedoraproject.org/taskmaster/builders/all/builds/4513/steps/runtask/logs/stdio"]],"name":"runtask","results":[0,[]],"statistics":{},"step_number":3,"text":["\'runtask","-i","...\'"],"times":[1408368416.582492,1408368416.975697],"urls":{}},{"eta":null,"expectations":[["output",1949,3510.0053185601414]],"hidden":false,"isFinished":true,"isStarted":true,"logs":[["stdio","http://taskotron-dev.fedoraproject.org/taskmaster/builders/all/builds/4513/steps/cat_log/logs/stdio"]],"name":"cat_log","results":[0,[]],"statistics":{},"step_number":4,"text":["\'cat","/var/log/taskotron/taskotron.log\'"],"times":[1408368416.976041,1408368416.990785],"urls":{}}],"text":["build","successful"],"times":[1408368415.947501,1408368416.991311]}'

simple_pass_data = {u'buildername' : u'all',
                    u'task_revision' : u'2b3a8895897efae1f93cfd94378247d6d9419d8e',
                    u'taskname' : u'rpmlint',
                    u'item' : u'krb5-1.12.1-9.fc21',
                    u'item_type' : u'koji_build',
                    u'time_triggered': u'Mon 18 Aug 2014 13:26:55',
                    u'slavename': u'taskotron-client28'}

simple_pass_data_properties = [[u'arch', u'noarch', u'Change'],
                 [u'branch', None, u'Build'],
                 [u'builddir', u'/home/buildslave/slave/all', u'slave'],
                 [u'buildername', u'all', u'Builder'],
                 [u'buildnumber', 4513, u'Build'],
                 [u'codebase', u'', u'Build'],
                 [u'got_revision',
                  u'2b3a8895897efae1f93cfd94378247d6d9419d8e',
                  u'Git'],
                 [u'item', u'krb5-1.12.1-9.fc21', u'Change'],
                 [u'item_type', u'koji_build', u'Change'],
                 [u'project', u'rpmcheck', u'Build'],
                 [u'repository', u'', u'Build'],
                 [u'revision', None, u'Build'],
                 [u'scheduler', u'jobsched-noarch', u'Scheduler'],
                 [u'slavename', u'taskotron-client28', u'BuildSlave'],
                 [u'taskname', u'rpmlint', u'Change'],
                 [u'workdir',
                  u'/home/buildslave/slave/all',
                  u'slave (deprecated)']]

simple_pass_data_properties_dict = {u'arch': 'noarch',
                                    u'branch': None,
                                    u'builddir': u'/home/buildslave/slave/all',
                                    u'buildername': u'all',
                                    u'buildnumber': 4513,
                                    u'codebase': u'',
                                    u'got_revision': u'2b3a8895897efae1f93cfd94378247d6d9419d8e',
                                    u'item': u'krb5-1.12.1-9.fc21',
                                    u'item_type': u'koji_build',
                                    u'project': u'rpmcheck',
                                    u'repository': u'',
                                    u'revision': None,
                                    u'scheduler': u'jobsched-noarch',
                                    u'slavename': u'taskotron-client28',
                                    u'taskname': u'rpmlint',
                                    u'workdir': u'/home/buildslave/slave/all'}


class TestBuildBot(object):

    def setup_method(self, method):
        self.buildbot_url = 'http://localhost/buildbot'
        self.builder = u'all'
        self.buildid = u'4513'

    def test_buildbot_get_builddata(self):
        stub_api_client = Dingus('api_request', get__returns = simple_pass_json)

        #loaded_buildinfo = json.loads(simple_pass_json)
        #pprint(loaded_buildinfo)

        buildbot_interface = buildbot.BuildbotInterface(self.buildbot_url, client=stub_api_client)
        test_data = buildbot_interface.get_buildbot_data(self.builder, self.buildid)

        assert test_data == simple_pass_data

    def test_buildbot_make_properties_dict(self):
        buildbot_interface = buildbot.BuildbotInterface(self.buildbot_url, client=Dingus())
        test_dict = buildbot_interface.make_buildbot_properties_dict(simple_pass_data_properties)

        assert test_dict == simple_pass_data_properties_dict
